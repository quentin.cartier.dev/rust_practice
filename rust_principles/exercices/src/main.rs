fn main() {
    let mut val : Vec<i32> = vec![1, 3, 5, 7];
    let res = my_function(val.clone());
    val.push(19);
    println!("{:?}", val);

    let mut val2 = 5;
    add_two(val2);
    println!("Res val2 : {}", val2);

    add_two_bis(&mut val2);
    println!("Res val2 : {}", val2);

    let b = 5;
    add_two_test(b);
}

fn my_function(val : Vec<i32>) -> bool {
    println!("{:?}", val);
    if val[0] == 1 {
        true
    } else {
        false
    }
}

fn add_two(arg0 :i8) {
    let mut arg0_2 = arg0;
    println!("Added 2 to {}", arg0_2);
    arg0_2 = arg0_2 +  2;
    println!("Now it is {}", arg0_2);
}

fn add_two_bis(arg0 :&mut i8) {
    let a : &mut i8 = arg0;
    *a = *a +  2;

    println!("Now it is {}", a);
}

fn add_two_test(val:i8) {
    val+2;
}