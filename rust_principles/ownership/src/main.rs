fn main() {
    let var = 1; // Pushed on the stack
    let mut s = "hello".to_string(); // Created on the heap
    s.push_str(", world");

    println!("var =  {}", var);
    println!("s = {}", s);

    // Move
    let x = vec!["mystring".to_string()];
    let y = x; 
    //println!("{:?}", x) => error: x value has been moved to y.
    println!("{:?}", y);


    // Clone : Perform a deep copy
    //      May be expensive
    let z = y.clone(); // OK we created a new variable
    println!("{:?}", z);

    // Copy : 
    // Most type implement a move, but some types may implement a copy: 
    // - Types that implements Copy trait are primitive types that are stored on the stack like integers and floats.
    let w = var;
    println!("var = {}, w = {}", var, w); // OK var uses copy


    // Example move with string
    let my_str = String::from("takes");
    takes_ownership(my_str);
    //println!("{}", my_str); => Error: my_str has no more ownership

    let mut my_str2 = String::from("takes and gives");
    my_str2 = takes_and_gives_ownership(my_str2);
    println!("{}", my_str2);

    let my_str3 = String::from("borrows");
    borrows_ownership(&my_str3);
    println!("{}", my_str3); // OK: Ownership has been given back

    // References and Borrowings
    // Ref : allows to make a reference without making an ownership.
    // 2 types of references : mutable and not mutable.

    let mut my_str4 = String::from("Hello");
    change_string(&mut my_str4); // We need to explicit that it is a mutable reference
    println!("{}", my_str4);


    


}
// car is dropped, s is dropped

fn takes_ownership(s: String) {
    let m_str = s;
    println!("Takes for : {}", m_str);
}

fn takes_and_gives_ownership(s: String)-> String {
    let m_str = s;
    println!("Takes and gives for : {}", m_str);
    m_str
}

fn borrows_ownership(s: &str) {
    let m_str: &str = s;
    println!("Borrows for : {}", m_str);
}

fn change_string(some_string: &mut String) {
    some_string.push_str(", world");
}