fn main() {
    let mut one = 1;

    
    'my_loop_name: loop{
        if one > 5 {
            println!("True {}", one);
        } else if one == 5 {
            println!("Equal {}", one)
        }else {
            println!("False {}", one);
        }
        one = one + 1;
        if one > 10 {
            break 'my_loop_name;
        }
    }

    let mut count1 = 0;
    while count1 < 5 {
        println!("Print while {}", count1);
        count1+=1;
    }

    for count2 in (0..10).rev() {
        println!("For-loop {}", count2);
    }

    let a = 5%2;
    println!("{}", a)
}
