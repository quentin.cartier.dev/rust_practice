fn main() {

    // Tuples

    let tup = (495, "hello world", false);
    println!("Tuple : {}, {}, {}", tup.0, tup.1, tup.2);
    println!("Tuple: {:?}", tup); //  ? => Debug
    println!("Tuple: {:X?}", tup); 
    println!("Tuple: {:b}", tup.0); 
    println!("Tuple: {:E}", tup.0); // UpperExp
    println!("Tuple: {:x}", tup.0); // LowerHex
    println!("Tuple: {:e}", tup.0); // LowerExp
    println!("Tuple: {:o}", tup.0); 

    let (x, y, z) = tup;
    println!("x = {}, y = {}, z = {}", x, y, z);

    // Arrays 
    // As for tuples , fixed length, but need to be same type.

    let array = [1, 2, 3];
    println!("{}", array[0]);

    let mut array2: [i32; 3] = [4, 5, 6];
    println!("{}", array2[0]);
    array2[0] = 10;
    println!("{}", array2[0]);

    // Vector3
    // Dynamic sized on the heap

    let mut nums = vec![1,2,3];
    nums.push(4);
    println!("vector: {:?}", nums); // use debug mode to print 
    nums.pop();
    println!("vector: {:?}", nums); // use debug mode to print 
    nums.reverse();
    println!("vector: {:?}", nums); // use debug mode to print 

    // We use clone as it may reverse in-place and destroy the original ordering
    let rev: Vec<_> =  nums.clone().into_iter().rev().collect(); 
    println!("Reversed: {:?} vs Original : {:?}", rev, nums); // use debug mode to print 

    // Using Vec::new()
    let mut vec = Vec::new(); //vec!
    vec.push(5);
    println!("vector (2): {:?}", vec); // use debug mode to print 


    let mut vect = Vec::<i32>::with_capacity(2);
    println!("Capacity: {}", vect.capacity());
    vect.push(5);
    vect.push(9);
    vect.push(10);
    println!("New capacity: {}", vect.capacity());
    vect.push(9);
    vect.push(10);
    println!("New capacity: {}", vect.capacity());

    //Using an iterator
    let v3: Vec<i32> = (3..10).collect();
    println!("Created with iterator : {:?}", v3);

    let nums2 = vec![(1..11)];
    println!("Vector = {:?}", nums2);
}
