fn main() {
    let x = 5;
    let mut y = 6;
    println!("Value of y is {}", y);
    //x = 6; => error not mutable
    y = 9;
    println!("Value of x is {}, and y is {}", x, y);

    // types

    let m_int: i8 = 19;
    println!("Integer value : {}", m_int);

    let decimal = 25_5;
    let hex = 0xff;
    let octal = 0o377;
    let binary = 0b1111_1111;

    println!("Are same = {}, {}, {}, {}", decimal, hex, octal, binary);

    // Bool 
    let m_bool = true;
    let m_bool2: bool = true;
    println!("Bool = {}, {}", m_bool, m_bool2);

    // Char 
    let c = 'c';
    println!("Char : {}", c);
}