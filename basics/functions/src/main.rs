
fn main() {

    // Convention : snake_case for functions
    
    
    print_phrase("TEST");

    // Greatest common denom.

    let res1 = gcd(5, 50);
    println!("{}", res1);

    println!("{}", multiple_return_values(true));

    let m_str = String::from("Hello");
    let res2 = concat_string(m_str);
    println!("{}", res2);
}

fn print_phrase(phrase: &str) {
    println!("Printed phrase: {}", phrase);
}

fn gcd(mut a: u64, mut b: u64) -> u64 {
    while a != 0 {
        if a < b {
            let c = a;
            a = b;
            b = c;
        }
        a = a % b
    }
    b
}

fn multiple_return_values(flag: bool) -> bool {
    if flag == true {
        true
    } else {
        false
    }
}

fn concat_string(arg1 : String) -> String {
    let res = arg1 + " World";
    res
}