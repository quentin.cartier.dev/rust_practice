fn main() {

    let name = String::from("MyString");
    let name2 = "Rust Name 2".to_string();

    let new_name = name.replace("String", "NewStr");

    println!("{}", name);
    println!("{}", name2);
    println!("{}", new_name);

    // &str = "string slice" or "stir"

    let str1 = "hello"; //&str
    let str2 = str1.to_string();
    let str3 = &str2;

    println!("str1 : {}, str2 : {}, str3 : {}", str1, str2, str3);

    // compare

    println!("is same = {}, {}, {}", str1 == str2, str2 == str3.to_string(), str1 == str3);

    println!("{}", "ONE".to_lowercase() == "one");

    // String literals
    let rust = "\x52\x75\x73\x74"; // Rust
    println!("{}", rust);
}
