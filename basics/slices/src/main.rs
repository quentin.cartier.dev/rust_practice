fn main() {
    let v: Vec<i32> = (0..10).collect();
    println!("Original vector : {:?}", v);

    // A slice points to a range of consecutives ( not owning)
    
    let sv1: &[i32] = &v;
    println!("slice : {:?}", sv1);

    let sv2: &[i32] = &v[2..4];
    println!("slice : {:?}", sv2);

    let sv3: &[i32] = &v[2..];
    println!("slice : {:?}", sv3);

    let sv4: &[i32] = &v[..5];
    println!("slice : {:?}", sv4);

}
