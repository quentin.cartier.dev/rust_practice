# Readme


```
# Install cargo-modules
cargo install cargo-modules
# Display to us the setup of our crate with the modules
cargo modules generate tree
# Even better, seeing structs and functions
cargo modules generate tree --with-types
```

Output for the last command:
```
crate todo
└── mod list: pub(crate)
    ├── struct Tasks: pub(self)
    ├── mod items_completed: pub(self)
    └── mod things_todo: pub(self)
```

THe pub keyword makes a item public, so it can be acces outside the module.
```pub(crate)``` is accessible anywhere inside the crate, but not outside.
```pub(self)``` if not pub keyword. It equivalent to private in other languages, and is the default.