fn main() {
    
    // Lifetimes anatation : to help detect dangling references.
    //Lifetimes 3 rules:
    // - Each paramter that is a reference gets its own lifetime params.
    // - If there it exactly one input lifetime parameter, 
    //      that lifetime is assigned to all output lifetime parameters.
    // - If multiples input lifetim params, but one of them os &self, the lifetime
    //      &self is assigned to all output lifetime parameters.

    let r;

    {
        let x = 5;
        // r = &x; => Error : x does not live long enough
        r = x;
    } // x is dropped

    println!("{}", r);


    let msg = "test";
    let msg2 = "test2";
    let (res, res2)  = example(msg, msg2);
    println!("{}, {}", res, res2);


    let r2: &u64;
    {
        let xyz: &'static u64 = &45;
        r2 = xyz;
    }

    println!("{}", r2);
}

fn example<'a,'b>(x: &'a str, y: &'b str) -> (&'a str, &'b str) {
    (x, y)
}