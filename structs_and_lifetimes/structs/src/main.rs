//struct name are in Camel case.
struct User {
    active: bool,
    username: String,
    sign_in_count: u32,
}

struct Square {
    width: u32,
    height: u32,
}

impl Square {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn get_width(&self)-> u32{
        self.width
    }

    fn get_height(&self)-> u32{
        self.height
    }

    fn change_width(&mut self, new_width: u32) {
        self.width = new_width;
    }
}

struct Coordinates(i32, i32, i32);


fn main() {
    // 3 kind of structs: named field, tuple like, and unit like

    let user1 : User = User{ active: true, username: String::from("FooName"), sign_in_count: 39};
    println!("User1 : {}, {}, {}", user1.active, user1.username, user1.sign_in_count);
    let user2 = build_user("hello");
    println!("User2 : {}, {}, {}", user2.active, user2.username, user2.sign_in_count);

    let coords = Coordinates(2,3,4);
    println!("Coords : {}, {}, {}", coords.0, coords.1, coords.2);

    let mut square = Square{width:12, height:8};
    square.change_width(2);
    println!("Square : w = {}, h = {}, area = {}", square.get_width(), square.get_height(), square.area());

}

fn build_user(name: &str) -> User {
    User {
        username: name.to_string(),
        active: true,
        sign_in_count: 39,
    }
}
