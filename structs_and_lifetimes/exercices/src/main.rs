/* 
Create a struct called Car with the fields: mpg, color, and top_speed. 
Once the struct is created, implement the following methods: set_mpg, set_color, and set_top_speed.
 Once you have created these methods, create a car, provide it default values, and then set the mpg, 
 color, and top speed and then print them out.
 */

#[derive(Debug)]
struct Car {
    mpg: String,
    color: String,
    top_speed: u32,
}

impl Car {
    fn set_mpg(&mut self, new_mpg: &str) {
        self.mpg = new_mpg.to_string();
    }

    fn set_color(&mut self, new_color: &str) {
        self.color = new_color.to_string();
    }

    fn set_top_speed(&mut self, new_speed: u32) {
        self.top_speed = new_speed;
    }
}

fn main() {

    let mut car = Car{mpg:String::from("TEST"), color: String::from("Purple"), top_speed:45};
    println!("Car : {}, {}, {}", car.mpg, car.color, car.top_speed);

    car.set_color("Pink");
    car.set_mpg("I don't know what it means");
    car.set_top_speed(30);
    println!("Car : {}, {}, {}", car.mpg, car.color, car.top_speed);
    println!("Car (with debug): {:?}", car);

}
