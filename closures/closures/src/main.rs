use rand::seq::SliceRandom;
use rand::thread_rng;
use text_colorizer::{self, Colorize};


#[derive(Debug)]
struct City {
    city: String,
    population: u64,
}

fn sort_popu(city: &mut Vec<City>) {
    city.sort_by_key(pop_helper)
}

fn pop_helper(pop: &City) -> u64 {
    pop.population
}

fn sort_with_closure(popu: &mut Vec<City>){
    popu.sort_by_key(|p| p.population)
}

fn get_city_names(popu: &Vec<City>) -> Vec<String> {
    popu.into_iter().map(|p| p.city.clone()).collect()
}


fn main() {
    let a = City{city: String::from("A"), population: 100};
    let b = City{city: String::from("B"), population: 15};
    let c = City{city: String::from("C"), population: 145};
    let d = City{city: String::from("D"), population: 1};
    let e = City{city: String::from("E"), population: 19};

    let mut vec: Vec<City> = Vec::new();
    vec.push(a);
    vec.push(b);
    vec.push(c);
    vec.push(d);
    vec.push(e);

    // Without closures
    sort_popu(&mut vec);
    println!("{}: {:?}", "Sorted without closure".yellow().bold(),  vec);

    vec.shuffle(&mut thread_rng());
    println!("{}: {:?}", "Shuffled".blue().bold(),  vec);

    // With closures
    sort_with_closure(&mut vec);
    println!("{}: {:?}", "Sorted with closure".green().bold(),  get_city_names(&vec));

    // Closures with type annotation
    let add = |x : i32 | -> i32 { x +1 };
    let add_v2 = | x | x + 1; // Let the compiler infer the type
    add_v2(1); 

    let test_infered = | x | x;
    let str = test_infered(String::from("string")); // OK : test_infered x has been infered as String
    //let num = test_infered(2); // Would not work : test_infered is alread infered as String.

    // Closures are not allocated in the heap.
    // Closure can avoid to call a separate function, very acceptable for performance optimization.

    // 3 Traits to covers : Fn FnMut, and FnOnce
    // Fn -> family of closures and functions that can be call multiple times without restrictions.
    //      - borrows value from the environment immutably
    //      - includes all fn functions
    // FnMut -> mutable
    // FnOnce -> 


    // Copy and clone for clusures
    let y = 5;
    let add_y = | x | x + y;
    let copy = add_y; // this is closure being copied
    println!("{}", add_y(copy(10)));


}
