fn main() {
    // 
    let a = (0 .. 5).fold(0, |sum, val |  sum + val);
    println!("Sum of values from 0 to 5 : {}", a);
    let b: i32 = (0 .. 5).fold(0, |sum, val: i32 | sum + val.pow(2));
    println!("Sum of squares from 0 to 5: {}", b);
}