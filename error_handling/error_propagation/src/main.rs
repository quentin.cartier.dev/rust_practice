use std::fs::File;
use std::io::ErrorKind;
use std::fs::rename;
use std::io::Error;

fn main() {
    let test = open_file();
    match test {
        Err(err) => match err.kind(){
            ErrorKind::NotFound => {
                File::create("error.txt").expect("Could not create non existing file");
            },
            _ => {},
        },
        _ => println!("No error"),
    }
    rename_file().expect("Could not rename file");
}

fn open_file() -> Result<File, Error> {
    let file = File::open("error.txt")?;
    Ok(file)
}

fn rename_file() -> Result<(), Error> {
    let file = rename("error.txt", "renamed.txt")?;
    Ok(file)
}
