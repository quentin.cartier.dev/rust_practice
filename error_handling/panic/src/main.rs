fn main() {
    // Panic marco is for unrecoverable errors
    // It will terminate the current thread

    // To get traces (in linux):
    // export RUST_BACKTRACE=1

    //panic!("Panicked!!");
    let v = vec![1];
    println!("{}", v[10]);

}
