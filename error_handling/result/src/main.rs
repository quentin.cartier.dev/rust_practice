use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let file = File::open("not_existing_file.txt");
    let _file = match file {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("file.txt") {
                Ok(file_created) => file_created,
                Err(err) => panic!("Cannot create the file! {:?}", err),
            }
            _ => panic!("Error of other kind")
        }
    };

    // Unwrap will panic if error, else gives the object.
    let file2 = File::open("file.txt").unwrap();

    let file3 = File::open("error.txt").expect("Error opening the file!");
}
