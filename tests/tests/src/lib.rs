pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }

    #[test]
    fn it_works_2() {
        let result = add(5,0);
        assert_eq!(result, 5);
    }

    #[test]
    fn file_exist() {
        use std::fs::File;
        File::open("some_file.txt").unwrap();

    }
}


