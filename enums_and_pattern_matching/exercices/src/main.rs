/* 
Create an enum called Shape and provide the values of "triangle square, pentagon, octagon".  
Then create a method for this enum that returns the number of corners each shape 
has based on the type of shape.

Example:
triangle.corners() will return 3
square.corners() will return 4
 */

enum Shape{
    Triangle,
    Square,
    Pentagon,
    Octagon
}

impl Shape {
    fn corners(&self) -> u8 {
        match self {
            Shape::Triangle => 3,
            Shape::Square => 4,
            Shape::Pentagon => 5,
            Shape::Octagon => 6
        }
    }
}

fn main() {
    println!("");
    let pentagon = Shape::Pentagon;
    println!("Triangle has {} corners.", Shape::Triangle.corners());
    println!("Square has {} corners.", Shape::Square.corners());
    println!("Pentagone has {} corners.", pentagon.corners());
    println!("Octagon has {} corners.", Shape::Octagon.corners());
}
