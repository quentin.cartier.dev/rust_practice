enum Pet {
    Dog,
    Cat,
    Fish
}

impl Pet {
    fn what(self) -> &'static str {
        match self {
            Pet::Dog => "I am a dog",
            Pet::Cat => "I am a cat",
            Pet::Fish => "I am a fish",
        }
    }
}

enum IpAddrKind {
    V4,
    V6
}

impl IpAddrKind {
    fn what(self) -> &'static str {
        match self {
            IpAddrKind::V4 => "V4",
            IpAddrKind::V6 => "V6"
        }
    }
}

// num inside a structure
struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

// We can use a more concise way to associate value to an enum.
// Enum with associated String values
#[derive(Debug)]
enum Fruit {
    Apple(String),
    Pear(String),
}

fn main() {
    let dog = Pet::Dog;
    println!("{}", dog.what());

    let ip_addr = IpAddr{ kind: IpAddrKind::V4 , address: String::from("192.168.0.1") };
    println!("{} :: {}", ip_addr.kind.what(), ip_addr.address);

    let apple = Fruit::Apple(String::from("AppleNumber1"));
    let apple2 = Fruit::Apple(String::from("AppleNumber2"));
    let pear3 = Fruit::Pear(String::from("PearNumber3"));

    println!("1: {:?}, 2: {:?}, 3: {:?}", apple, apple2, pear3);

}
