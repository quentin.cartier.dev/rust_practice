
struct V4 {
    str : String,
}

struct V6 {
    str : String,
}

enum IpAddr{
    V4(String),
    V6(String),
}

fn main() {
    
    let addrs = vec![
        IpAddr::V4(String::from("1234")),
        IpAddr::V6(String::from("1234")),
        IpAddr::V4(String::from("1234")),
        IpAddr::V6(String::from("34"))
    ];

    for item in addrs {
        let res = get_string_or_none(item);
        if res.is_none(){
            println!("Failed");
        }else{
            println!("Worked");
        }
    }

    let num = 5;
    let num_plus_1 = plus_one(Some(5));
    if num_plus_1.is_none() {
        println!("In None");
    } else{
        println!("{} plus one = {}", num , num_plus_1.unwrap());
    }

    println!("Should be 1 if None as input : {}",plus_one(None).unwrap());
    let m_v4 = IpAddr::V4("sss".to_string());

    if let IpAddr::V4(_) = m_v4 {
        println!("Some(5) == 5");
    }
    
}

//Option is defined like this:
/*
enum Option<T> {
    None,
    Some(T)
}
*/


// Return string only if V4
fn get_string_or_none(ip_addr: IpAddr) -> Option<String> {
    match ip_addr {
        IpAddr::V4(_) => Some(String::from("ABCD")),
        IpAddr::V6(_) => None
    }
}

fn plus_one(arg: Option<u32>) -> Option<u32> {
    match arg {
        None => Some(1),
        Some(i) => Some(i+1)
    }
}