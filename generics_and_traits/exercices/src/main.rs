
/* 
 (1) Modify the solution to the Section 4 assignment by creating a Trait that has the set_mpg, 
set_color, and set_top_speed methods. Then create a Motorcycle struct with the same fields as 
the Car struct: mpg, color, and top_speed. Now implement your Trait on both the Car and Motorcycle 
struct. Print out the results to confirm a working solution.
 */

/* 
 (2) Create a simple print function that uses Generic T. This Generic T will need to implement
  std::fmt::Debug depending on the values you pass in. Our function takes one parameter of type T.
    Our function will then print out the value that is passed in.
 */

trait Vehicle {
    fn set_mpg(&mut self, new_mpg: &str);
    fn set_color(&mut self, new_color: &str);
    fn set_top_speed(&mut self, new_top_speed: u32);
}

struct Car {
    mpg: String,
    color: String,
    top_speed: u32
}

struct Motorcycle {
    mpg: String,
    color: String,
    top_speed: u32
}

impl Default for Motorcycle {
    fn default() -> Self {
        Motorcycle { mpg: "Default Moto".to_string(), color: "silver".to_string(), top_speed:64}
    }
}

fn new_car() -> Car {
    Car {
        mpg: "none".to_string(),
        color: "none".to_string(),
        top_speed: 0
    }
}

impl Vehicle for Car {
    fn set_mpg(&mut self, mpg: &str) {
        self.mpg = mpg.to_string();
    }

    fn set_color(&mut self, color: &str) {
        self.color = color.to_string();
    }

    fn set_top_speed(&mut self, top_speed: u32) {
        self.top_speed = top_speed;
    }
}

impl Vehicle for Motorcycle {
    fn set_mpg(&mut self, mpg: &str) {
        self.mpg = mpg.to_string();
    }

    fn set_color(&mut self, color: &str) {
        self.color = color.to_string();
    }

    fn set_top_speed(&mut self, top_speed: u32) {
        self.top_speed = top_speed;
    }
}

#[derive(Debug)]
#[allow(dead_code)]
struct Item1{
    value: String
}

#[derive(Debug)]
#[allow(dead_code)]
struct Item2{
    content: String,
    rate: f32,
}

fn print_anything<T : std::fmt::Debug>(item: &T){
    println!("item : {:?}", item);
}

fn main() {
    let mut car = new_car();
    car.set_mpg("new_mpg");
    car.set_color("blue");
    car.set_top_speed(30);

    let mut moto = Motorcycle::default();
    moto.set_color("Grey");
    moto.set_top_speed(333);
    moto.set_mpg("Moto");

    println!("Car mpg : {}, color : {}, top_speed : {}", car.mpg, car.color, car.top_speed);
    println!("Moto mpg : {}, color : {}, top_speed : {}", moto.mpg, moto.color, moto.top_speed);


    
    let i1 = Item1{value: String::from("Item1")};
    let i2 = Item2{content: String::from("Item2"), rate: 4.3};
    print_anything(&i1);
    print_anything(&i2);
}
