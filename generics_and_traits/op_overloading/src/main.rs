
use std::ops::Add;


struct Point<T> {
    x: T,
    y: T,
}

impl<T> Add for Point<T>
    where 
    T: Add<Output =  T> {
        type Output = Self;
        fn add(self, rhs: Self) -> Self {
            Point{
                x: self.x + rhs.y,
                y: self.y + rhs.x,
            }

        }
    }

fn main() {
    let pt1 = Point{x: 5.0, y: 3.2};
    let pt2 = Point{x: 13.3, y: 1.4};

    let sum = pt1 + pt2;
    println!("Sum : {}, {}", sum.x, sum.y);

}
