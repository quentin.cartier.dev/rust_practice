struct Point<T> {
    x: T,
    y: T,
}

struct Pair<T, U> {
    a: T,
    b: U
}

fn main() {
    let my_point = Point{x:0.5 , y:0.7};
    println!("My point : {}, {}", my_point.x, my_point.y);

    let my_point2 = Point{x:"A", y:"B"};
    println!("My point2 : {}, {}", my_point2.x, my_point2.y);

    let my_pair = Pair{a:"ValueA", b:42};
    println!("My pair : {}, {}", my_pair.a, my_pair.b);
}
