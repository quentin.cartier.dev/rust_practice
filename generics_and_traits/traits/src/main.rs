
trait Overview{
    fn overview(&self) -> String;
    fn overview2(&self) -> String {
        format!("[DefaultImpl]")
    }
}

struct Course{
    headline: String,
    author: String,
}

struct AnotherCourse{
    headline: String,
    author: String,
}

impl Overview for Course{
    fn overview(&self) -> String {
        format!("{}, {}", self.author, self.headline)
    }
    fn overview2(&self) -> String {
        format!("{}, {}", self.author, self.headline)
    }
}

impl Overview for AnotherCourse{
    fn overview(&self) -> String {
        format!("{}, {}", self.author, self.headline)
    }
}

fn main() {
    let course = Course{author: String::from("Antoine"), headline: String::from("Carrots' Book")};
    let course2 = AnotherCourse{author: String::from("Arthur"), headline: String::from("Pens and Ananas")};
    let course2b = AnotherCourse{author: String::from("Arthur"), headline: String::from("Pens and Ananas 2")};

    println!("Course overview : {}", course.overview());
    println!("Another course overview: {}", course2.overview());
    println!("Course overview 2 : {}", course.overview2());
    println!("Another course overview 2: {}", course2.overview2());

    call_overview(&course);
    call_overview(&course2);

    call_overview2(&course, &course2);

    //call_overview3(&course, &course2); => Error: not the same type
    call_overview3(&course2, &course2b);
}

fn call_overview(item: &impl Overview) {
    println!("Overview : {} ", item.overview());
}

// Here we can have two different types implementing the Overview trait
fn call_overview2(item1: &impl Overview, item2: &impl Overview) {
    println!("[Overview2]");
    println!("\tItem1 : {}", item1.overview());
    println!("\tItem2 : {}", item2.overview());
}


// Now, we force item1 and item2 to have the same type and implementing the trai Overview
fn call_overview3<T : Overview>(item1: &T, item2: &T) {
    println!("[Overview3]");
    println!("\tItem1 : {}", item1.overview());
    println!("\tItem2 : {}", item2.overview());

}
