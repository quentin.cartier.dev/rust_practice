struct Course {
    author: String,
    headline: String,
}

impl Drop for Course {
    fn drop(&mut self) {
        println!("Dropped author {} of  course : {}", self.author, self.headline);
    }
}



fn main() {
    println!("*** The \'Drop\' trait ***");


    let course1 = Course{headline : String::from("Course1"), author: String::from("Billy")};
    drop(&course1);
}
