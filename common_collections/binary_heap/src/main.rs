use std::collections::BinaryHeap;

fn main() {
    // Binary heap : A coolection whose elements are kept loosely organized.
    // Greatest value always bubble up to the front.
    // Useful in cases where we care about priority.

    // 3 commonly used methods : push() , pop(), and peek()
    let mut bheap = BinaryHeap::new();


    bheap.push(1);
    bheap.push(18);
    bheap.push(20);
    bheap.push(5);
    bheap.push(19);

    // Is 20 at the front? It should be.
    println!("BHeap : {:?}", bheap);

    let popped = bheap.pop(); // Take off value to be gone.
    println!("Popped at the front : {:?}", popped);
    println!("Now bheap : {:?}", bheap);

    // Peak retrieve the value at front, but unlike pop(), does not pop it from the bheap.
    let peeked = bheap.peek();
    println!("Peeked at the front : {:?}", peeked);
    println!("Now bheap : {:?}", bheap);
}
