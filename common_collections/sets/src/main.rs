use std::collections::HashSet;

fn main() {
    // As for maps, there is 2 kinds of sets : Hhsh sets and B-tree sets
    let mut hs = HashSet::new();
    hs.insert(String::from("Test1"));
    hs.insert(String::from("Test2"));
    hs.insert(String::from("Test666"));
    hs.insert(String::from("Test666")); //do nothing
    hs.insert(String::from("Test8"));
    hs.insert(String::from("Test9"));
    hs.remove("Test9");

    let mut hs2 = HashSet::new();
    hs2.insert(String::from("Test8"));
    hs2.insert(String::from("No"));
    hs2.insert(String::from("Yes"));
    hs2.insert(String::from("Test2"));


    for x in hs.iter() {
        println!("Iter : {}", x);
    }
    for x in hs2.iter() {
        println!("Iter2 : {}", x);
    }

    let intersection_hs = &hs & &hs2;
    for x in intersection_hs.iter() {
        println!("Intersection hs : {}", x);
    }

    let union_hs = &hs | &hs2;
    for x in union_hs.iter() {
        println!("Union hs : {}", x);
    }


}
