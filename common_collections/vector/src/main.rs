use rand::seq::SliceRandom;
use rand::thread_rng; // RaNdom Generator
use rand::distributions::Uniform;
use rand::Rng;

fn main() {
    let mut nums:Vec<i32> = vec![];

    // Add values to a vector
    nums.push(1);
    nums.push(2);
    nums.push(3);

    // Remove values from a vector
    let popped = nums.pop();
    println!("Pooped value : {}", popped.unwrap());

    // Copy value
    let two = nums[1];
    // Reference to a value in the vector
    let two_ref = &nums[1];

    println!("Two : {}, Two ref : {}", two, two_ref);

    // Return first element as an Option<T>
    let one = nums.first();

    println!("One : {} ({:?})", one.unwrap(), one);

    // Borrowing mutable references with .first_mut, .last_mut
    let one_mut = nums.first_mut();
    if let Some( s) = one_mut {
        *s = 9;
    }

    println!("First is not one anymore  ==> {:?}", nums.first());

    // Length
    println!("Lenght of vector : {}", nums.len());
    println!("Is empty ? : {}", nums.is_empty());

    // Another way to add values to vetor is with 'insert'
    // It allows to insert at the index of our choice.
    nums.insert(1, 18);
    println!("Nums : {:?}", nums);

    // Sorting 
    nums.sort();
    println!("Sorted nms : {:?}", nums);

    //Reverse
    nums.reverse();
    println!("Reversed nms : {:?}", nums);


    // Random generation of elements
    let range = Uniform::new(5, 20);
    let mut vals: Vec<i32> = rand::thread_rng().sample_iter(&range).take(15).collect();
    println!("Random vector : {:?}", vals);

    // Shuffle
    // Using rand crate
    vals.shuffle(&mut thread_rng());
    println!("Shuffled random vec : {:?}", vals);


}
