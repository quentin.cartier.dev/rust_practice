use std::collections::HashMap;

fn main() {

    //In rust : 2 types of map : hash map, and B-tree map.
    // Difference reside in how the 2 maps keep entires arranged for their loopup.
    // hashmap : stored in a single heap allocated table.
    // B-tree map: stored in a tree like structure growing from the top down into many leaves ( nodes).

    let mut hm = HashMap::new();

    hm.insert(1, 1);
    hm.insert(42, 18);
    hm.insert(666, 19);

    println!("{:?}", hm);

    let mut hm2 = HashMap::new();
    
    hm2.insert(String::from("Hello"), String::from("World"));
    hm2.insert(String::from("Pikachu"), String::from("Not a lizard"));

    println!("{:?}", hm2);

    // Value updated if key already exist
    hm2.insert(String::from("Hello"), String::from("No"));
    println!("{:?}", hm2);

    // Contains key
    println!("From key {}, exist ? --> {}", String::from("Hello"), hm2.contains_key("Hello"));
    println!("From key {} --> {:?}", String::from("Hello"), hm2.get("Hello"));
    // Now remove
    let removed_value = hm2.remove("Hello");
    println!("From key {}, exist ? --> {}", String::from("Hello"), hm2.contains_key("Hello"));
    println!("From key {} --> {:?}", String::from("Hello"), hm2.get("Hello"));
    println!("{:?}", hm2);
    println!("Removed value --> {:?}", removed_value);

    // Now remove entry ( will return the remove entry pair)
    if  hm2.contains_key("Pikachu") {
        println!("Contains key : \"Pikachu\", removing it");
    }
    let removed_pair = hm2.remove_entry("Pikachu");
    println!("Removed entry --> {:?}", removed_pair);
    println!("Is hm empty now ? --> {}", hm2.is_empty());

   

}
